package com.joshafeinberg.contactreminder;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class PreferencesActivity extends FragmentActivity {

    public static final String VIBRATE_ON = "pref_key_vibrate";
    public static final String SMS_INCOMING = "pref_key_sms_incoming";
    public static final String PHONE_INCOMING = "pref_key_phone_incoming";
    public static final String PHONE_OUTGOING = "pref_key_phone_outgoing";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_add);

        getFragmentManager().beginTransaction().replace(R.id.reminder_detail_container, new PreferencesFragment()).commit();
    }
}
