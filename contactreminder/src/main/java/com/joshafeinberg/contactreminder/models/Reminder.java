package com.joshafeinberg.contactreminder.models;

import android.content.Context;

import com.joshafeinberg.contactreminder.databases.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Reminder {
    public List<ReminderItem> reminders;
    public List<String> contactList;
    public HashMap<String, List<Reminder.ReminderItem>> contactReminders;

    public Reminder(Context context, String contactID) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        reminders = databaseHelper.getRemindersForContact(contactID);
        //createMap();
    }

    public Reminder(Context context) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        reminders = databaseHelper.getAllReminders();
        createMap();
    }

    public void createMap() {
        contactList = new ArrayList<String>();
        String currentID = "";
        contactReminders = new HashMap<String, List<Reminder.ReminderItem>>();
        List<Reminder.ReminderItem> contactRemindersList = new ArrayList<Reminder.ReminderItem>();

        //Log.i("contactreminder", "Reminders Size => " + reminders.size());
        for (ReminderItem reminderItem : this.reminders) {
            if (currentID.equals("")) {
                contactList.add(reminderItem.contactID);
                currentID = reminderItem.contactID;
            }
            if (!currentID.equals(reminderItem.contactID)) {
                contactReminders.put(currentID, contactRemindersList);
                contactList.add(reminderItem.contactID);
                contactRemindersList = new ArrayList<Reminder.ReminderItem>();
                currentID = reminderItem.contactID;
            }
            contactRemindersList.add(reminderItem);
        }
        contactReminders.put(currentID, contactRemindersList);
    }

    public static class ReminderItem {
        public String reminderID;
        public String contactID;
        public String lookupID;
        public String reminder;
        public long dueDate;

        public ReminderItem(String reminderID, String contactID, String lookupID, String reminder, long dueDate) {
            this.reminderID = reminderID;
            this.contactID = contactID;
            this.lookupID = lookupID;
            this.reminder = reminder;
            this.dueDate = dueDate;
        }

        public String toString() {
            return this.reminder;
        }
    }

}