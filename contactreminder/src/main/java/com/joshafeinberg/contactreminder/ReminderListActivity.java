package com.joshafeinberg.contactreminder;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class ReminderListActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_list);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        /*ReminderListFragment reminderListFragment = (ReminderListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.expandableList);
        registerForContextMenu(reminderListFragment.expandableListView);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reminder_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addReminder:
                addReminder();
                break;
            case R.id.action_preference:
                showPreferences();
                break;
            case R.id.action_aboutApp:
                aboutApp();
                break;
        }

        return true;
    }


    private void addReminder() {
        Intent detailIntent = new Intent(this, AddReminderActivity.class);
        startActivity(detailIntent);
    }

    private void showPreferences() {
        Intent prefIntent = new Intent(this, PreferencesActivity.class);
        startActivity(prefIntent);
    }

    private void aboutApp() {
        View aboutView = LayoutInflater.from(this).inflate(R.layout.about_app, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.about);
        builder.setView(aboutView);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }
}
