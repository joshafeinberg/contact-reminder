package com.joshafeinberg.contactreminder.services;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;

import com.joshafeinberg.contactreminder.AddReminderFragment;
import com.joshafeinberg.contactreminder.PreferencesActivity;
import com.joshafeinberg.contactreminder.R;
import com.joshafeinberg.contactreminder.ReminderListActivity;
import com.joshafeinberg.contactreminder.databases.DatabaseHelper;
import com.joshafeinberg.contactreminder.models.Reminder;

import java.io.IOException;

public class NotifListenerService extends BroadcastReceiver {

    private Context mContext;
    private String mPhoneNumber;

    private class ContactInfo {
        public final String contactID;
        public final String contactName;

        private ContactInfo(String contactID, String contactName) {
            this.contactID = contactID;
            this.contactName = contactName;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        mContext = context;

        Bundle receivedBundle = intent.getExtras();
        String action = intent.getAction();
        if (action == null) {
            return;
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean smsIncoming = sharedPref.getBoolean(PreferencesActivity.SMS_INCOMING, true);
        boolean phoneIncoming = sharedPref.getBoolean(PreferencesActivity.PHONE_INCOMING, true);
        boolean phoneOutgoing = sharedPref.getBoolean(PreferencesActivity.PHONE_OUTGOING, true);

        // sms received
        if (action.contains(Telephony.Sms.Intents.SMS_RECEIVED_ACTION) && smsIncoming) {
            SmsMessage[] messages;
            if (receivedBundle != null) {
                Object[] pdus = (Object[]) receivedBundle.get("pdus");
                messages = new SmsMessage[pdus.length];
                for (int i = 0; i < messages.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    mPhoneNumber = messages[i].getOriginatingAddress();
                }
            }
            respondToMessage();
            return;
        }

        // TODO: sms sent
        // is this necessary? the issue I have is that you would need to send multiple in a row
        // perhaps can add but will need to use

        // phone call received
        if (action.contains(TelephonyManager.ACTION_PHONE_STATE_CHANGED) && phoneIncoming) {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (state != null && state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                mPhoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            }
            respondToMessage();
            return;
        }

        // phone call dialed
        if (action.contains(Intent.ACTION_NEW_OUTGOING_CALL) && phoneOutgoing) {
            mPhoneNumber = getResultData();
            if (mPhoneNumber == null) {
                mPhoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            }
            respondToMessage();
        }

        // due date arrived
        if (action.contains(AddReminderFragment.CUSTOM_ACTION)) {
            if (receivedBundle == null) {
                return;
            }
            long reminderID = receivedBundle.getLong("reminderID");
            DatabaseHelper databaseHelper = DatabaseHelper.getInstance(mContext);
            Reminder.ReminderItem reminderItem = databaseHelper.getReminderById(reminderID);
            AddReminderFragment.ContactDetails contactDetails =
                    AddReminderFragment.getContactDetailsByID(mContext, reminderItem.contactID,
                            reminderItem.lookupID);

            buildDueDateNotification(reminderID, contactDetails.contactPhotoThumbnailURI,
                    contactDetails.contactName, reminderItem.reminder);
        }

    }

    public ContactInfo getContactInfoByNumber() {

        if (mPhoneNumber == null) {
            return null;
        }

        Uri contentFilterUI = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
        if (contentFilterUI == null) {
            return null;
        }

        Uri uri = Uri.withAppendedPath(contentFilterUI, Uri.encode(mPhoneNumber));
        if (uri == null) {
            return null;
        }

        ContactInfo contactInfo = null;

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                contactInfo = new ContactInfo(
                        contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID)),
                        contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return contactInfo;
    }

    private void respondToMessage() {
        ContactInfo contactInfo = getContactInfoByNumber();

        if (contactInfo == null) {
            return;
        }

        Reminder reminder = new Reminder(mContext, contactInfo.contactID);
        if (reminder.reminders.size() == 0) {
            return;
        }

        String reminders = "";
        for (Reminder.ReminderItem reminderItem : reminder.reminders) {
            reminders += reminderItem.reminder + "\n";
        }
        // remove the last newline character
        reminders = reminders.substring(0, reminders.length() - 1);

        buildReminderNotification(contactInfo.contactID, contactInfo.contactName, reminders);
    }

    /**
     * produces the reminder if a contact is found to have any
     * @param contactID        contact id
     * @param contactName      contacts real name
     * @param reminderMessage  string of reminders
     */
    public void buildReminderNotification(final String contactID, String contactName, String reminderMessage) {

        // might want to switch this to a toast dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.reminderfor) + " " + contactName);
        builder.setMessage(reminderMessage);
        builder.setNegativeButton(R.string.snooze, null);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DatabaseHelper databaseHelper = DatabaseHelper.getInstance(mContext);
                databaseHelper.deleteRemindersForContact(contactID);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean vibrateOn = sharedPref.getBoolean(PreferencesActivity.VIBRATE_ON, true);

        if (!vibrateOn) {
            Log.i("contactreminder", "Vibration Off");
            return;
        }
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator.hasVibrator()) {
            // vibrate phone 3 times for 500ms, pausing 500ms
            long[] pattern = {0, 500, 500, 500, 500, 500, 500};
            vibrator.vibrate(pattern, -1);
        } else {
            Log.i("contactreminder", "Vibrator Not Found");
        }
    }

    public void buildDueDateNotification(long reminderID, String thumbnailURI, String contactName, String reminder) {

        Bitmap bitmap = null;
        try {
            if (thumbnailURI != null && !thumbnailURI.equals("")) {
                bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), Uri.parse(thumbnailURI));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(contactName)
                .setContentText(reminder)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);

        if (bitmap != null) {
            builder.setLargeIcon(bitmap);
        }

        Intent resultIntent = new Intent(mContext, ReminderListActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) reminderID, builder.build());



    }


}
