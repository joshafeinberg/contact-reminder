Add a reminder to a certain contact. When you receive a phone call, go to call them, or receive an SMS message from that person an alert will remind you.

Also you can set a due date so if you don't talk for a bit you will still get the reminder.

New version has been created with new design, better functionality, and Wear support.

Created to be an open source project located at https://bitbucket.org/joshafeinberg/contact-reminder and released on the MIT license.

Downloadable from [Google Play](https://play.google.com/store/apps/details?id=com.joshafeinberg.contactreminder2)